﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using notes.Controllers;

namespace notes
{
    public class RoutingMiddleware
    {
        private readonly RequestDelegate _next;

        public RoutingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)   
        {
            var path = httpContext.Request.Path.Value.ToLower();
            
            switch (path)
            {
                case "/home":
                    await HomeController.HandleHomePage(httpContext);
                    break;

                case "/":
                    await HomeController.HandleHomePage(httpContext);
                    break;

                case "/registration":
                    await UserController.HandleRegistration(httpContext);                  
                    break;
                
                case "/authentication":
                    await UserController.HandleAuthentication(httpContext);
                    break;

                case "/make_note":
                    await NoteController.HandleMakingNote(httpContext);                 
                    break;

                case "/load_note":
                    await NoteController.HandleLoadingNote(httpContext);                    
                    break;

                case "/notes":
                    await NoteController.HandleNotesList(httpContext);                 
                    break;
                
                default:
                    if (path.StartsWith("/txt_files"))                   
                        await HomeController.HandleStaticFiles(httpContext);                   
                    else 
                        httpContext.Response.StatusCode = 404;                  
                    break;
            }                               
        }
    }
}