using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace notes
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            await _next.Invoke(context);

            switch (context.Response.StatusCode)
            {
                case 401:
                    await context.Response.WriteAsync("Unauthorized user!");
                    break;
                case 404:
                    await context.Response.WriteAsync("Page not found!");
                    break;
            }
        }
    }
}