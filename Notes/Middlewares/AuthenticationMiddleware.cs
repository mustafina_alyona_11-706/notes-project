using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace notes
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            var path = httpContext.Request.Path.Value.ToLower();
            
            var cookies = httpContext.Request.Cookies;
            
            if (string.IsNullOrEmpty(cookies["login"]) 
                && httpContext.Request.Path != "/registration" 
                && httpContext.Request.Path != "/authentication")   
                
                httpContext.Response.Redirect("/authentication");                    
            else           
                await _next.Invoke(httpContext);                        
        }
    }
}