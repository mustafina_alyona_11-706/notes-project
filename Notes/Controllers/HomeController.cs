using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Linq;
using notes.Views;

namespace notes.Controllers
{
    public class HomeController
    {  
        public static async Task HandleHomePage(HttpContext httpContext)
        {
            await ViewHelper.GetResponsePage(httpContext, "home");
        }   

        public static async Task HandleStaticFiles(HttpContext httpContext)
        {
            var title = httpContext.Request.Query["title"];
            using (var dbContext = new DatabaseContext())
            {
                var note = dbContext.Notes.FirstOrDefault(n => n.Title == title);
                var text = (note == null) ? "Error" : note.Text;
                await httpContext.Response.WriteAsync(text);
            }
        }     
    }
}