using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using notes.Views;

namespace notes.Controllers
{
    public class UserController
    {
        private static readonly UserRepository UserRepository = new UserRepository();
        
        public static async Task HandleRegistration(HttpContext httpContext)
        {
            if (httpContext.Request.Method == HttpMethods.Post)
            {
                var login = httpContext.Request.Form["login"];
                var password = httpContext.Request.Form["password"];
                var userSaved = UserRepository.TrySave(login, password);
                if (userSaved) await ViewHelper.GetResponsePage(httpContext, "success");
                else await ViewHelper.GetResponsePage(httpContext, "failure");
            }
            else if (httpContext.Request.Method == HttpMethods.Get)
                await ViewHelper.GetResponsePage(httpContext, "registration");
        }
        
        public static async Task HandleAuthentication(HttpContext httpContext)
        {
            if (httpContext.Request.Method == HttpMethods.Post)
            {
                var login = httpContext.Request.Form["login"];
                var password = httpContext.Request.Form["password"];
                var userExists = UserRepository.CheckUserInDb(login, password);
                if (userExists)
                {
                    httpContext.Response.Cookies.Append(
                        "login",
                        login,
                        new CookieOptions
                        {
                            Expires = DateTimeOffset.UtcNow.AddHours(15)
                        });
                    await ViewHelper.GetResponsePage(httpContext, "success");
                }

                httpContext.Response.StatusCode = 401;
            }
            else if (httpContext.Request.Method == HttpMethods.Get)
                await ViewHelper.GetResponsePage(httpContext, "authentication");
        }
    }
}