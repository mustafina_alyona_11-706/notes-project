using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using notes.Views;

namespace notes.Controllers
{
    public class NoteController
    {
        private static readonly NoteRepository NoteRepository = new NoteRepository();
        
        public static async Task HandleMakingNote(HttpContext httpContext)
        {
            if (httpContext.Request.Method == HttpMethods.Post)
            {
                var title = httpContext.Request.Form["title"];
                var text = httpContext.Request.Form["text"];
                NoteRepository.Save(
                    new Note{Title = title, Text = text}
                );
                await ViewHelper.GetResponsePage(httpContext, "success");
            }
            else if (httpContext.Request.Method == HttpMethods.Get)
                await ViewHelper.GetResponsePage(httpContext, "make_note");
        }

        public static async Task HandleLoadingNote(HttpContext httpContext)
        {
            if (httpContext.Request.Method == HttpMethods.Post)
            {
                var file = httpContext.Request.Form.Files["load"];
                var title = file.FileName;
                var text = File.ReadAllText($@"txt_files/{file.FileName}");
                NoteRepository.Save(
                    new Note{Title = title, Text = text}
                );
                await ViewHelper.GetResponsePage(httpContext, "success");
            }
            else if (httpContext.Request.Method == HttpMethods.Get)
                await ViewHelper.GetResponsePage(httpContext, "load_note");
        }
        
        public static async Task HandleNotesList(HttpContext httpContext)
        {
            var cookie = httpContext.Request.Cookies["login"];
            if (cookie == null)
            {
                httpContext.Response.StatusCode = 401;
                return;
            }
            else
            {
                using (var dbContext = new DatabaseContext())
                {
                    var notes = dbContext.Notes;
                    var titles = notes.Select(n => n.Title);
                    var stringBuilder = new StringBuilder();
                    foreach (var title in titles)
                    {
                        var link = $@"<a href=""txt_files?title={title}"">{title}</a>";
                        stringBuilder.Append(link);
                        stringBuilder.Append("<br/><br/>");
                    }

                    var result = File.ReadAllText(@"Templates/notes.html").Replace("@notes", stringBuilder.ToString());
                    await httpContext.Response.WriteAsync(result);
                }
            }                    
        }
    }
}