using System;
using System.Collections.Generic;
using System.Linq;

namespace notes
{
    public class UserRepository: IRepository<User>, IDisposable
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        
        public void Save(User user)
        {
            _db.Users.Add(user);
            _db.SaveChanges();
        }

        public IEnumerable<User> List()
        {
            return _db.Users.ToList();
        }

        public User Get(int id)
        {
            return _db.Users.Find(id);
        }

        public void Dispose()
        {
            _db?.Dispose();
        }
        
        public bool CheckUserInDb(string login, string password)
        {
            var exists = false;
            var user = _db.Users.FirstOrDefault(u => u.Login == login & u.Password == password);
            if (user != null) exists = true;
            return exists;
        }
        
        public bool TrySave(string login, string password)
        {
            var logins = _db.Users.Select(u => u.Login);
            if (logins.Contains(login)) return false;
            Save(new User(login, password));
            return true;
        }      
    }
}