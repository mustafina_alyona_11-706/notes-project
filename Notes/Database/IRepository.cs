using System.Collections.Generic;

namespace notes
{
    public interface IRepository<T>
    {
        void Save(T obj);
        IEnumerable<T> List();
        T Get(int id);
    }
}