using System;
using System.Collections.Generic;

namespace notes
{
    public class NoteRepository : IDisposable, IRepository<Note>
    {
        private readonly DatabaseContext _db = new DatabaseContext();

        public void Save(Note note)
        {
            _db.Notes.Add(note);
            _db.SaveChanges();
        }

        public IEnumerable<Note> List()
        {
            return _db.Notes;
        }

        public Note Get(int id)
        {
            return _db.Notes.Find(id);
        }

        public void Dispose()
        {
            _db?.Dispose();
        }
    }
}