using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace notes.Views
{
    public class ViewHelper
    {
        public static async Task GetResponsePage(HttpContext context, string fileName)
        {
            var path = $@"Templates/{fileName}.html";
            var htmlPage = File.ReadAllText(path);
            await context.Response.WriteAsync(htmlPage);
        }  
    }
}