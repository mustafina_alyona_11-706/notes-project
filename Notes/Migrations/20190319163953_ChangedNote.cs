﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace notes.Migrations
{
    public partial class ChangedNote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Notes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "Notes",
                nullable: false,
                defaultValue: 0);
        }
    }
}
